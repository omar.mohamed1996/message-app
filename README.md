## Content:
* How to run the project on your computer
* API
    * Apply a long celery task and show all the status
    * Adding a Message
    * Get celery task info by id
    
## Installation / Run 

### Local on your computer
Step 1) Check that you are at the level that contains the env/ directory (the same level that also has this README file) and run app by this command:
    `make deploy`

Step 2) Run Redis open new Terminal:
    
    1. Activate .venv by this command: `source .venv/bin/activate` 
    2. then run celery: `celery -A worker:celery worker --loglevel=DEBUG`
        
### Docker
Step 1) install [Docker & Docker-compose](https://docs.docker.com/compose/install/)

Step 2) Run `docker-compose build`

Step 3) Run `docker-compose up`


## API

I used Postman to Interface all Api Requests by import [message_app collection](https://gitlab.com/omar.mohamed1996/message-app/-/blob/main/message_app.postman_collection.json).


Endpoint | Methods | Rule
---|---|---
home.index    |  GET |      /
messages.index| POST  |   /messages/
static        |  GET   |   /static/<path:filename>
status.get    |  GET   |   /status/<task_id>/

#### Apply a long celery task and show all the status
![](./readme_images/1.png)

![](./readme_images/2.png)
![](./readme_images/3.png)
![](./readme_images/4.png)
![](./readme_images/5.png)

![](./readme_images/6.png)

#### Adding a Message
![](./readme_images/7.png)

#### Get celery task info by id
![](./readme_images/8.png)

