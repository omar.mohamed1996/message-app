FROM python:3.7.6-buster


WORKDIR /usr/src/
COPY . .

RUN pip install --upgrade pip
COPY ./app/requirements.txt /usr/src/requirements.txt
RUN pip install -r requirements.txt
COPY wsgi.py /usr/src/wsgi.py

EXPOSE 5000
CMD ["python", "wsgi.py"]