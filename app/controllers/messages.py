from turtle import delay
from flask import Blueprint
from flask import request, jsonify
from app.tasks.long_task import insert_message, get_all_message
from app.extensions import db
from app.models.message import Message
from flask import url_for

messages = Blueprint("messages", __name__, url_prefix="/messages")


@messages.route("/", methods=['POST'])
def index():
    if request.method == 'POST':
        message = request.form.get('message')
        
        if message != None:
            message_status = insert_message.apply_async(args=[message])
            
            return jsonify({
                
                "message": message,
                "task": url_for("status.get", task_id=message_status.id, _external=True),
                
                "status":"Success Added to Queue"
                }), 201

        return "Please Enter your message", 400